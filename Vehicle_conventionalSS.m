function [mdot_f] = Vehicle_conventionalSS(v_veh,a_veh,engine_type)

% Initialize Vehicle Parameters:
run initFiles/initConventional

% VEHICLE DYNAMICS
% Wheel speed (rad/s)
wv  = v_veh ./r_w;
% Wheel acceleration (rad/s^2)
dwv = a_veh ./r_w;
% Rolling Friction:
Fr = m_veh*cr*g;
% Aerodynamic Drag
Fa = rho*Cd.*v_veh.^2*Af;

% Wheel torque (Nm)
Tv = (Fr + Fa + m_veh.*a_veh) .* r_w;




% TRANSMISSION
% gear ratios
for i = 1:length(v_veh)
    if v_veh(i)<gear_speed(2)
        r_gear(i) = gear_ratio(1);
    elseif v_veh(i)>=gear_speed(2)&& v_veh(i)<gear_speed(3)
        r_gear(i) = gear_ratio(2);
    elseif v_veh(i)>=gear_speed(3)&& v_veh(i)<gear_speed(4)
        r_gear(i) = gear_ratio(3);
    elseif v_veh(i)>=gear_speed(4)&& v_veh(i)<gear_speed(5)
        r_gear(i) = gear_ratio(4);
    elseif v_veh(i)>=gear_speed(5)
        r_gear(i) = gear_ratio(5);
    end
end
clear i
% r_gear =  interp1(gear_speed,gear_ratio,v_veh,'previous');
% Crankshaft speed (rad/s)
wg  = TR*r_gear .* wv;
% Crankshaft acceleration (rad/s^2)
dwg = TR*r_gear .* dwv;
% Crankshaft torque (Nm)
Tg_p = (Tv>0).* Tv ./ r_gear ./ eta_trans/TR;  % Traction
Tg_n = (Tv<=0) .* Tv ./ r_gear.* 0.95/TR;      % Braking
Tg  = Tg_p+Tg_n;

mdot_f   = zeros(size(v_veh));
TICE     = zeros(size(v_veh));
Taux     = zeros(size(v_veh));
wICE     = zeros(size(v_veh));
% Auxiliaries 
P_aux = 400;    %[W]
for i = 1:length(v_veh)
    wICE(i) = max(w_IDLE,wg(i));
    T_aux(i) = P_aux./wICE(i);
    TICE_max    = interp1(maxT_w,maxT_T,wICE(i),'linear*','extrap');
    TICE(i)     =  min(Tg_p(i)+T_aux(i),TICE_max);
    TICE(i)     = max(TICE(i),T_IDLE);
    mdot_f(i) = interp2(map_w,map_T,map_m_dot',wICE(i),TICE(i))*1e-3;
    % Deceleration Fuel Cut-off
    if wICE(i)<=0||TICE(i)<=0
        mdot_f(i) = 0;
    end
    if isnan(mdot_f(i))
        mdot_f(i) = 0;
    end
end






