%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Description:
% This script computes the personal transportation energy consumption of a
% household (multiple individuals)
%
% Created by M. Muratori 2012
%
% INPUT: n, Activity (from RPD)
% OUTPUT: Fuel consumption [l] and electric energy consumption [kWh] for
% personal tranposrtation. % Trip matrix table (columns content): 
% Time step in which the trip starts, Trip duration (minutes),
% Trip length (kilometers), Flag(1=leisure, 2/3=commute to/from work,
% 4=charging availability at home, 5=charging availability at work), liquid
% fuel consumption [liters], Electricity consumption, Final SOC, Failed
% trips
%
% UPDATES: S.Stockar (05/06/2015)
% - Fixed problem on the number of people/adults driving
% - Fixed issue with tecnology adoption (single type of vehicle for all the
% adult in the household)
% - Converted hev and pev models in matlab
% - Developed new models for conventional (diesel and gasoline engine) and
% electric
% - Updated outputs
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all 
close all
clc
%% Read the Household Characteristics from Lapis and save the parameters in a structure
load houseSampleUnstr % This line will be eliminated in Lapis

for i=1:length(names)
    eval(strcat('HHCParameters.',names{i},'= ',names{i},';'));
end
clearvars -except HHCParameters % This line will be eliminated in Lapis

%% Load reasults from other models
load RPD_Output % Calculated for House = 1
clearvars -except HHCParameters Activity
houseNumber = 1; % Cannot change this number in the example because RPD Output

%% Calculate the Transportation Energy Demand
% HHCParameters.vehicleType(houseNumber) = 5; %test with different types of vehicles
tripMatrix = TEC(Activity, HHCParameters,houseNumber);

%% Calculate energy demand for PEV Charging
[totalPowerPEV, enablingPEV, deadlinePEV, completionPEV,initialSOCPEV] = computeResultsPEV(tripMatrix,HHCParameters,Activity,houseNumber);

