% -------------------------------------------------------------------------
% Description: Generate the avtivity patterns for the batch Simulation
%
% Created:          06/30/2015
% Author:           S.Stockar
% Last Modified:
%--------------------------------------------------------------------------

clear all
close all
clc

% Collect the information from the RPD
load Activity_100houses1week
Activity_All    = Activity;
% Calculate the number of Adults considered in this simulation
n_tot   = 1;


global Dur_state Dis_state D_state R_state P_D P_R P_ij_combine_u P_ij_combine_h v_state a_state v_Markov_index

%% Load data and initialize global variables

% Load parameters for generating velocity profiles
load initFiles\Profile_Parameters;

% Load data on the commuting characteristics
run initFiles\householdCharacteristics

% Calculate the simulation window (same as the RPD)
day     = size(Activity_All,2)/144;


%% Swwep for each House
for k = 1:size(Activity_All,1)
    Activity = Activity_All(k,:);
    n = N(k,:);
    Typology_flag=[];
    for i = 1:4     % Only considers Adults
        Typology_flag=[Typology_flag,ones(1,n(i))*i];
    end
    clear i
    % Select the vehicle type 1=CV,2=HEV,3=PHEV,4=EV, 5 = no car
    % In case of conventional vehicles, differentiate between gasoline or
    % diesel
    [Veh_Type, engine_type] = techAdoption(n_tot);
    
    %% Generate Trip Matrix
    %Generate trip matrix: Time step in which the trip starts, Trip duration, Trip mileage, Flag(1=leisure, 2/3=commute to/from work, 4=charging availability at home, 5=charging availability at work)
    %Generate associated velocity profiles
    for person=1:n_tot
        A=Activity(person,:);
        working_flag = Typology_flag(person);
        %Commute for leisure
        Trip_matrix_temp=zeros(length(A),8);
        j=1;
        i=1;
        while i < length(A)
            if A(i)<8
                Trip_matrix_temp(j,1)=i; % Time step in which the person start being at home
                ii=1;
                while (i+ii<length(A) && A(i+ii)<8)
                    ii=ii+1;
                end
                Trip_matrix_temp(j,2)=ii; % Number of time steps the person spend at home = can charge
                Trip_matrix_temp(j,4)=4; % 4= flag for being at home
                j=j+1; % Move to the next event in the trip matrix
                i=i+ii; % Move to the next time step in the activity pattern
            elseif A(i)==8
                Trip_matrix_temp(j,1)=i+1; % Time step in which the person start being at work (+1 because the first time 8 appears the person in travelling to work)
                ii=1;
                while i+ii<length(A) && A(i+ii)==8
                    ii=ii+1;
                end
                Trip_matrix_temp(j,2)=ii-1; % Number of time steps the person spend at work = can charge
                Trip_matrix_temp(j,4)=5; % 5= flag for being at work
                j=j+1;
                i=i+ii;
            elseif A(i)==9
                Trip_matrix_temp(j,1)=i;
                ii=1;
                while i+ii<length(A) && A(i+ii)==9
                    ii=ii+1;
                end
                Trip_duration = round(ii*unifrnd(0,1)); % Trip duration is a uniform between 0 and 100% of the time spend doing activity away-not working
                if Trip_duration >0 % Remove trips that have duration 0 minutes
                    Trip_matrix_temp(j,2)=Trip_duration;
                    Trip_matrix_temp(j,4)=1; % 1= flag for leisure trip
                    
                end
                j=j+1;
                i=i+ii;
            elseif i<length(A)
                i=i+1;
            end
        end
        % Remove lines with all zeros, First column is the time step at which each trip happens, the second column is the duration of the trip, in time steps, the third is trip mileage and the fourth a flag
        Trip_matrix_temp(~any(Trip_matrix_temp,2),:)=[];
        % Convert trip duration from time-steps to minutes
        Trip_matrix_temp=[Trip_matrix_temp(:,1),Trip_matrix_temp(:,2)*10,Trip_matrix_temp(:,3),Trip_matrix_temp(:,4),Trip_matrix_temp(:,5),Trip_matrix_temp(:,6),Trip_matrix_temp(:,7),Trip_matrix_temp(:,8)];
        
        % Commute to work, for 2 commute to work per each day a workers
        % do activity 8
        WorkCommuteClass=rando(DISTWork); % Per each person we drawn a commute to work distance. Given the class the distance in miles is found with uniform distribution inside that class
        Trip_matrix_temp2=zeros(length(A),8);
        if working_flag < 3 % If the person modeled is a worker (1=WM,2=WF,3=NWM,4=NWF,5=C)
            WorkCommute=unifrnd(DISTLow(WorkCommuteClass),DISTUpp(WorkCommuteClass)); % Daily commute to work in miles
            WorkCommute=(WorkCommute*1.609); %Convert From Miles to km
            for i=1:day
                Activity_day=Activity(person,((i-1)*144+1:(i-1)*144+144));
                DayType = (mod(i,7)>1)+1; %1 for holidays and 2 per working days
                if DayType == 2
                    to_work = 1;
                    from_work=144;
                    if sum( Activity_day == 8) >0 % If the person goes to work in the current day
                        while Activity_day(to_work) ~= 8 % find the first time step in which the person goes to work
                            to_work=to_work+1;
                        end
                    end
                    Trip_matrix_temp2((i-1)*144+to_work,1)=(i-1)*144+to_work;
                    Trip_matrix_temp2((i-1)*144+to_work,3)=WorkCommute;
                    Trip_matrix_temp2((i-1)*144+to_work,4)=2; % 2= flag for trip to work
                    if sum( Activity_day == 8) >0 % If the person goes to work in the current day
                        while Activity_day(from_work) ~= 8 % find the last time step in which the person is at work
                            from_work=from_work-1;
                        end
                    end
                    Trip_matrix_temp2((i-1)*144+from_work,1)=(i-1)*144+from_work;
                    Trip_matrix_temp2((i-1)*144+from_work,3)=WorkCommute;
                    Trip_matrix_temp2((i-1)*144+from_work,4)=3; % 3= flag for trip to work
                end
            end
        else
            WorkCommute = 0; %Accounting for non working people
        end
        WorkCommuteAll(person)=(WorkCommute); % Save the commute to work distance for this person,in km
        Trip_matrix_temp2(~any(Trip_matrix_temp2,2),:)=[]; %Remove lines with all zeros, First column is the time step at which each trip happens, the second column is the duration of the trip, in time steps, the third is trip mileage and the fourth a flag
        
        Trip_matrix=sortrows([Trip_matrix_temp;Trip_matrix_temp2],1);
        %Generate velocity profiles
        tic
        %     disp(person)
        j=1;
        for i = 1: length(Trip_matrix)
            if Trip_matrix(i,4) == 2
                Trip_velocity{person,j} = distance2profile(Trip_matrix(i,3)); % generate the velocity profile of all trips of distance in k
                j=j+1;
            elseif Trip_matrix(i,4) == 3
                Trip_velocity{person,j} = distance2profile(Trip_matrix(i,3)); % generate the velocity profile of all trips of distance in k
                j=j+1;
            elseif Trip_matrix(i,4)==1
                Trip_velocity{person,j} = duration2profile(Trip_matrix(i,2));
                Trip_matrix(i,3)=trapz((Trip_velocity{person,j})/3600);
                j=j+1;
            end
        end
        for i = (length(Trip_matrix)+1) : 1500
            Trip_velocity{person,i}=0;
        end
        toc
        Trip_matrix_All{person}=Trip_matrix; % Array vector containing a matrix for each person where columns are: Time step in which the trip starts, Trip duration (minutes), distance driven in km, Flag(1=leisure, 2/3=work commute)
    end
    
%     clearvars -except k Trip_matrix_All n_tot Veh_Type Trip_velocity Charger Activity Trip_matrix engine_type PowerChargingHome PowerChargingWork
    
    %% Transportation Energy
    
    for person=1:n_tot
        vehicleType=Veh_Type(person);
        disp(person)
        tic
        Trip_matrix=cell2mat(Trip_matrix_All(person));
        if Veh_Type(person) ==1
            disp('Conventional Vehicle');
            run initFiles/initConventional
            FuelConsumption_C  = zeros(n_tot,1500);
            MPG_C              = zeros(n_tot,1500);
            %         Final_SOC_HEV=zeros(n_tot,1500);
            tripIndex=1;
            for i=1:length(Trip_matrix)
                if Trip_matrix(i,4) == 1 || Trip_matrix(i,4) ==  2 || Trip_matrix(i,4) == 3 % Flag = 1,2,3 are trips, 4 and 5 are charge availability
                    v_generate  = Trip_velocity{person,tripIndex};
                    V_generate  = [1:1:length(v_generate);v_generate];
                    Time        = 1:1:length(v_generate);
                    V_veh       = v_generate/3.6;
                    A_veh       = gradient(V_veh);
                    % Run the vhehicle Model
                    [mdot_f] = Vehicle_conventionalSS(V_veh,A_veh,engine_type);
                    if length(v_generate) > 1
                        FuelConsumption_C(person,tripIndex)   = sum(mdot_f)/0.77; % Liter of gasoline
                        MPG_C(person,tripIndex)               = trapz(V_veh)/(1000*1.6)/(sum(mdot_f)/0.77*0.264172);
                        Trip_matrix(i,5)                      = FuelConsumption_C(person,tripIndex); %Fuel consumption in liters
                    end
                    tripIndex=tripIndex+1;
                end
            end
            
        end
        if Veh_Type(person) ==2
            disp('Hybrid Electric Vehicle');
            run initFiles/initHEV
            FuelConsumption_HEV = zeros(n_tot,1500);
            MPG_HEV             = zeros(n_tot,1500);
            Final_SOC_HEV       = zeros(n_tot,1500);
            tripIndex=1;
            for i=1:length(Trip_matrix)
                if Trip_matrix(i,4) == 1 || Trip_matrix(i,4) ==  2 || Trip_matrix(i,4) == 3 % Flag = 1,2,3 are trips, 4 and 5 are charge availability
                    v_generate  = Trip_velocity{person,tripIndex};
                    V_generate  = [1:1:length(v_generate);v_generate];
                    Time        = 1:1:length(v_generate);
                    V_veh       = v_generate/3.6;
                    A_veh       = gradient(V_veh);
                    % Run the vhehicle Model
                    [BATT_SOC,mdot_f] = Vehicle_hevSS(V_veh,A_veh);
                    if length(v_generate) > 1
                        Final_SOC_HEV(person,tripIndex)         = BATT_SOC(end);
                        FuelConsumption_HEV(person,tripIndex)   = sum(mdot_f)/0.77; % Liter of gasoline
                        MPG_HEV(person,tripIndex)               = trapz(V_veh)/(1000*1.6)/(sum(mdot_f)/0.77*0.264172);
                        SOC0                                    = Final_SOC_HEV(person,tripIndex); % Reinitialize SOC
                        Trip_matrix(i,5)                        = FuelConsumption_HEV(person,tripIndex); %Fuel consumption in liters
                        Trip_matrix(i,7)                        = Final_SOC_HEV(person,tripIndex); % Reinitialize SOC
                    end
                    tripIndex=tripIndex+1;
                end
            end
        end
        if Veh_Type(person) ==3
            disp('Plug In Electric Vehicle');
            run initFiles/initPEV
            FuelConsumption_PEV = zeros(n_tot,1500);
            MPG_PEV             = zeros(n_tot,1500);
            MPG_PEV_eq          = zeros(n_tot,1500);
            FuelConsumption_PEV_eq = zeros(n_tot,1500);
            Final_SOC_PEV       = zeros(n_tot,1500);
            tripIndex           = 1;
            rho_gas             = 0.77; % [kg/l]
            Qlhv                = 44; % Lower heating value for gasoline [kJ/g]
            for i=1:length(Trip_matrix)
                if Trip_matrix(i,4) == 1 || Trip_matrix(i,4) ==  2 || Trip_matrix(i,4) == 3 % Flag = 1,2,3 are trips, 4 and 5 are charge availability
                    v_generate  = Trip_velocity{person,tripIndex};
                    V_generate  = [1:1:length(v_generate);v_generate];
                    Time        = 1:1:length(v_generate);
                    V_veh       = v_generate/3.6;
                    A_veh       = gradient(V_veh);
                    % Run the vhehicle Model
                    [BATT_SOC,mdot_f] = Vehicle_pevSS(V_veh,A_veh);
                    if length(v_generate) > 1
                        Final_SOC_PEV(person,tripIndex)         = BATT_SOC(end);
                        FuelConsumption_PEV(person,tripIndex)   = sum(mdot_f)/0.77; % Liter of gasoline
                        FuelConsumption_PEV_eq(person,tripIndex)= (BATT_SOC(1)-BATT_SOC(end))*BattEmax*3.6e6/(Qlhv*rho_gas); %convert from kWh to equivalent liters of gasoline
                        MPG_PEV(person,tripIndex)               = trapz(V_veh)/(1000*1.6)/(sum(mdot_f)/rho_gas*0.264172);
                        MPG_PEV_eq(person,tripIndex)            = trapz(V_veh)/ (FuelConsumption_PEV(person,tripIndex)+FuelConsumption_PEV_eq(person,tripIndex));
                        SOC0                                    = Final_SOC_PEV(person,tripIndex); % Reinitialize SOC
                        Trip_matrix(i,5)                        = FuelConsumption_PEV(person,tripIndex); %Fuel consumption in liters
                        Trip_matrix(i,6)                        = (BATT_SOC(1)-BATT_SOC(end))*BattEmax; % Electric Energy from the Battery in kWh
                        Trip_matrix(i,7)                        = Final_SOC_PEV(person,tripIndex); % Reinitialize SOC
                    end
                    tripIndex=tripIndex+1;
                elseif Trip_matrix(i,4) == 4 % Charging at home
                    ChargeAvailability= Trip_matrix(i,2)*60; %Number of seconds for which vehicle is plugged in
                    SOC_charging=SOC0;
                    ChargingTime=0;
                    while SOC_charging < 0.95 && ChargingTime < ChargeAvailability
                        SOC_charging = SOC_charging + PowerChargingHome /(3.6e6*BattEmax);
                        ChargingTime = ChargingTime+1;
                    end
                    SOC0=SOC_charging;
                    Trip_matrix(i,6) = -1*PowerChargingHome*ChargingTime/3600000; % Electric Energy from the grid in kWh
                    Trip_matrix(i,7) = SOC_charging; % Reinitialize SOC
                    
                elseif Trip_matrix(i,4) == 5 % Charging at work
                    ChargeAvailability= Trip_matrix(i,2)*60; %Number of seconds for which vehicle is plugged in
                    SOC_charging=SOC0;
                    ChargingTime=0;
                    while SOC_charging < 0.95 && ChargingTime < ChargeAvailability
                        SOC_charging = SOC_charging + PowerChargingWork /(3.6e6*BattEmax);
                        ChargingTime = ChargingTime+1;
                    end
                    SOC0=SOC_charging;
                    Trip_matrix(i,6) = -1*PowerChargingWork*ChargingTime/3600000; % Electric Energy from the grid in kWh
                    Trip_matrix(i,7) = SOC_charging; % Reinitialize SOC
                    
                end
                
            end
        end
        if Veh_Type(person) ==4
            disp('Electric Vehicle');
            run initFiles/initEV
            FuelConsumption_EV = zeros(n_tot,1500);
            MPG_EV             = zeros(1,n_tot);
            Final_SOC_EV       = zeros(n_tot,1500);
            tripIndex           = 1;
            for i=1:length(Trip_matrix)
                if Trip_matrix(i,4) == 1 || Trip_matrix(i,4) ==  2 || Trip_matrix(i,4) == 3 % Flag = 1,2,3 are trips, 4 and 5 are charge availability
                    v_generate  = Trip_velocity{person,tripIndex};
                    V_generate  = [1:1:length(v_generate);v_generate];
                    Time        = 1:1:length(v_generate);
                    V_veh       = v_generate/3.6;
                    A_veh       = gradient(V_veh);
                    % Run the vhehicle Model
                    [BATT_SOC] = Vehicle_evSS(V_veh,A_veh);
                    if length(v_generate) > 1
                        Final_SOC_EV(person,tripIndex)          = BATT_SOC(end);
                        SOC0                                    = Final_SOC_EV(person,tripIndex); % Reinitialize SOC
                        Trip_matrix(i,6)                        = (BATT_SOC(1)-BATT_SOC(end))*BattEmax; % Electric Energy from the Battery in kWh
                        Trip_matrix(i,7)                        = BATT_SOC(end); % Reinitialize SOC
                    end
                    tripIndex=tripIndex+1;
                elseif Trip_matrix(i,4) == 4 % Charging at home
                    ChargeAvailability= Trip_matrix(i,2)*60; %Number of seconds for which vehicle is plugged in
                    SOC_charging=SOC0;
                    ChargingTime=0;
                    while SOC_charging < 0.95 && ChargingTime < ChargeAvailability
                        SOC_charging = SOC_charging + PowerChargingHome /(3.6e6*BattEmax);
                        ChargingTime = ChargingTime+1;
                    end
                    SOC0=SOC_charging;
                    Trip_matrix(i,6) = -1*PowerChargingHome*ChargingTime/3600000; % Electric Energy from the grid in kWh
                    Trip_matrix(i,7) = SOC_charging; % Reinitialize SOC
                    
                elseif Trip_matrix(i,4) == 5 % Charging at work
                    ChargeAvailability= Trip_matrix(i,2)*60; %Number of seconds for which vehicle is plugged in
                    SOC_charging=SOC0;
                    ChargingTime=0;
                    while SOC_charging < 0.95 && ChargingTime < ChargeAvailability
                        SOC_charging = SOC_charging + PowerChargingWork /(3.6e6*BattEmax);
                        ChargingTime = ChargingTime+1;
                    end
                    SOC0=SOC_charging;
                    Trip_matrix(i,6) = -1*PowerChargingWork*ChargingTime/3600000; % Electric Energy from the grid in kWh
                    Trip_matrix(i,7) = SOC_charging; % Reinitialize SOC
                    
                end
                
            end
        end
        Trip_matrix_All{person}=Trip_matrix;
    end
    Trip_matrix_OUT{k}  = Trip_matrix;
    Veh_Type_OUT(k)     = Veh_Type;
end
clearvars -except Trip_matrix_OUT Veh_Type_OUT
