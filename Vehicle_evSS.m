function [BATT_SOC] = Vehicle_evSS(v_veh,a_veh)

% Initialize Vehicle Parameters:
run initFiles/initPEV

% VEHICLE DYNAMICS
% Wheel speed (rad/s)
wv  = v_veh ./r_w;
% Wheel acceleration (rad/s^2)
dwv = a_veh ./r_w;
% Rolling Friction:
Fr = m_veh*cr*g;
% Aerodynamic Drag
Fa = rho*Cd.*v_veh.^2*Af;
% Wheel torque (Nm)
Tv = (Fr + Fa + m_veh.*a_veh) .* r_w;

% Wheel Power
Pv = Tv.*wv;
% TRANSMISSION
% gear ratios
for i = 1:length(v_veh)
    if v_veh(i)<gear_speed(2)
        r_gear(i) = gear_ratio(1);
    elseif v_veh(i)>=gear_speed(2)&& v_veh(i)<gear_speed(3)
        r_gear(i) = gear_ratio(2);
    elseif v_veh(i)>=gear_speed(3)&& v_veh(i)<gear_speed(4)
        r_gear(i) = gear_ratio(3);
    elseif v_veh(i)>=gear_speed(4)&& v_veh(i)<gear_speed(5)
        r_gear(i) = gear_ratio(4);
    elseif v_veh(i)>=gear_speed(5)
         r_gear(i) = gear_ratio(5);
    end
end
clear i
% r_gear =  interp1(gear_speed,gear_ratio,v_veh,'previous');
% Crankshaft speed (rad/s)
wg  = TR*r_gear .* wv;
% Crankshaft acceleration (rad/s^2)
dwg = TR*r_gear .* dwv;
% Crankshaft torque (Nm)
Tg_p = (Tv>0).* Tv ./ r_gear ./ eta_trans/TR;  % Traction
Tg_n = (Tv<=0) .* Tv ./ r_gear.* 0.95/TR;      % Braking
Tg  = Tg_p+Tg_n;

% SPEEDS
wEM  = wv*GR_EM;

BATT_SOC    = zeros(size(v_veh));
BATT_SOC(1) = SOC0;
for i = 1:length(v_veh)
    if  BATT_SOC(i)>= SOC_ref
        if Tv(i)<0 % Regenerative Braking
            TEM_max = interp1(EMmaxT_w,EMmaxT_T,wEM(i),'linear',min(EMmaxT_T));
            TEM_min = interp1(EMmaxT_w,EMminT_T,wEM(i),'linear',max(EMminT_T));
            TEM(i)  = max(Tv(i)/GR_EM,TEM_min);
        else
            TEM_max = interp1(EMmaxT_w,EMmaxT_T,wEM(i),'linear',min(EMmaxT_T));
            TEM_min = interp1(EMmaxT_w,EMminT_T,wEM(i),'linear',max(EMmaxT_T));
            TEM(i)      = max(min(Tv(i)/GR_EM,TEM_max),TEM_min);
            Tdiff       = (Tv(i)-TEM(i)*GR_EM) ./ r_gear(i).* 0.95/TR;
        end
        eff_EM  = interp2(EMmap_w,EMmap_T*EMscale,EMmap_eff',wEM(i),abs(TEM(i)),'linear', 0.5);
        if TEM(i)>=0
            eff_EM = 1/eff_EM;
        end
        if TEM(i)>=0
            eff_batt = 1/eta_batt;
        else
            eff_batt = eta_batt;
        end
        PEM     = max(min(BattPmax*1e3,TEM(i)*wEM(i)*eff_EM*eff_batt),-BattPmax*1e3);
        BATT_SOC(i+1) = BATT_SOC(i)-PEM/(3.6e6*BattEmax);
    end
end






