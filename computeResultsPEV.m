function [totalPowerPEV, enablingPEV, deadlinePEV, completionPEV, initialSOCPEV] = computeResultsPEV(tripMatrix,HHCParameters,Activity,houseNumber)
enablingPEV     = [];
deadlinePEV     = [];
completionPEV   = [];
initialSOCPEV   = [];
temp_PEV = zeros(length(tripMatrix),length(Activity));

for i = 1:length(tripMatrix)
    temp = cell2mat(tripMatrix(:,i));
    [enabling, deadline, complete,SOC0] = ComputePEV_para(temp,HHCParameters,Activity,houseNumber);
    index = find(enabling==4);
    enablingPEV     = [enablingPEV index];
    deadlinePEV     = [deadlinePEV deadline(index)];
    completionPEV   = [completionPEV complete(index)];
    initialSOCPEV   = [initialSOCPEV SOC0];
    for ii = 1:length(temp_PEV)
        if deadline(ii)>0 && complete(ii)>0
            temp_PEV(i,ii) = HHCParameters.powerPEVHome(houseNumber);
        end
    end
end
totalPowerPEV = sum(temp_PEV,1);


