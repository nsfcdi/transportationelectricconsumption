function [velocityProfile v] = distance2profile(distance)

% Generates the velocity profile (expressed in km/h paced in seconds)
% corresponding to a certain input distance (expressed in km)

global P_R;             % Probability distribution of highway ratio (0-1) vs. trip distance (km) 
global P_ij_combine_u;  % Urban transition probabilities matrix from velocity i-th to j-th at a certain acceleration value (92 quantized velocities and 57 quantized accelerations)
global P_ij_combine_h;  % Highway transition probabilities matrix from velocity i-th to j-th at a certain acceleration value (139 quantized velocities and 56 quantized accelerations)
global v_state;         % Velocities state vector
global a_state;         % Accelerations state vector
global v_Markov_index;  % Current Markov chian index

%% Highway Ratio Generation

if (distance >= 200)
    j = 41;                             % The last distance class
else
    j = round(distance/5) + 1;          % Find the closest upper distance class    
end
HW_ratio = 1/20*(rando(P_R(:,j))-1);    % The highway ratio is drawn from the probability distribution of highway ratio (0-1) vs. trip distance (km) 
                                        % The linear transformation 1/20*(x-1) is to go from the distribution into the highway classes with pace 0.05

%% Sections generation 

Highway_p = HW_ratio;                         % Highway ratio  
Urban_p1 = (rand*0.9 + 0.1)*(1 - Highway_p);  % Urban ratio of the 1st section
Urban_p2 = 1 - Urban_p1 - Highway_p;          % Urban ratio of the 2nd section  

%% Fisrt urban section

S = v_state;                        % Velocities state space    
mu = [1 zeros(1,91)];               % Velocities initial distribution with probability 1 the velocity class at the beginning is the first one
v_Markov_index = rando(mu);         % Generate the first value of state vector
driving_dis = Urban_p1*distance;    % Section driving distance 
a_urban = 0;                        % Current urban acceleration initialization
dis = 0;                            % Current distance variable initialization (meters)
t = 1;                              % Current time variable initialization

while dis <= 0.98*driving_dis*1000  % Compare the current distance with the section distance      
    
    [~,indexEntry] = min(abs(a_urban-a_state)); % Find the right discretized acceleration in the acceleration vector
    P = P_ij_combine_u(:,:,indexEntry);         % Select the correct transitions probability matrix
        
    if sum(P(v_Markov_index(t),:))~=0                 % Chek whether or not the transition probabilies are all null
        if t < 5                                      % For the first 5 times increases the probability of the transition from velocity i-th to i+1-th so that the vehicle accelerates
            P(v_Markov_index(t),v_Markov_index(t)+1) = P(v_Markov_index(t),v_Markov_index(t)+1) + 0.5;
        end
        v_Markov_index(t+1) = rando(P(v_Markov_index(t),:));  % Compute the next step of the Markov chain
    else                                              % In the case that the transition probabilies are all null  
        v_Markov_index(t+1) = v_Markov_index(t);      % The velocity Markov index is kept constant 
    end
    
    dis = sum(S(v_Markov_index(1:t)))/3.6;      % Update the current duration (from km/h per second to meters)    
    a_urban = (S(v_Markov_index(t+1))-S(v_Markov_index(t)))/3.6; % Update the current urban acceleration (from km/h per second to meter/s per second)
    
    if a_urban > 5                                    % When acceleration is greater than 5.2 m/s^2
        v_Markov_index(t+1) = v_Markov_index(t)+19;   % Velocity increases at most of 18.7 km/h
    elseif a_urban < -7                               % When deceleration is greater than 6.6 m/s^2
        v_Markov_index(t+1) = v_Markov_index(t)-24;   % Velocity decreases at most of 23.8 km/h
    end                                               % Acceleration/deceleration values from the recorded data   
    
    [~,index] = min(abs(a_urban-a_state));            % Chose the closest state among the available ones
    a_urban = a_state(index);                         % Assign the closest discretized acceleration to the current acceleration     
    
    t = t+1;                                          % Increase time of 1 second
    
end
v = v_Markov_index;
v_generate1 = S(v_Markov_index(1:t));                                               % Assign velocity state vector to generated profile 
v_generate1 = [v_generate1 v_generate1(end):-3:0 zeros(1,round(unifrnd(0,1)*20))];  % Terminate the velocity profile 

%% Highway section

S = v_state;                            % Velocities state space
mu = [1 zeros(1,91)];                   % Velocities initial distribution with probability 1 the velocity class at the beginning is the first one    
v_Markov_index = rando(mu);             % Generate the first value of state vector
driving_dis = Highway_p*distance;       % Driving distance for this section
a_highway = 0;                          % Current urban acceleration initialization
dis = 0;                                % Current distance variable initialization (meters)
t = 1;                                  % Current time variable initialization

while dis <= 0.98*driving_dis*1000      % Compare the current distance with the section distance
    
    [~,indexEntry] = min(abs(a_highway - a_state));   % Find the right discretized acceleration in the acceleration vector
    P = P_ij_combine_h(:,:,indexEntry);               % Select the correct transitions probability matrix
    
    if sum(P(v_Markov_index(t),:))~=0                 % Chek whether or not the transition probabilies are all null
        if t < 5                                      % For the first 5 times increases the probability of the transition from velocity i-th to i+1-th so that the vehicle accelerates                                    
            P(v_Markov_index(t),v_Markov_index(t)+1) = P(v_Markov_index(t),v_Markov_index(t)+1)+0.5;
        end
        
        if (driving_dis*1000-dis < 300)&&(v_Markov_index(t) > 5) % In the last 300 meters increases the probability of the transition from velocity i-th to i-1-th so that the vehicle deacelerates  
            P(v_Markov_index(t),v_Markov_index(t)-1)=P(v_Markov_index(t),v_Markov_index(t)-1)+0.5;
        end
        v_Markov_index(t+1) = rando(P(v_Markov_index(t),:)); % Compute the next step of the Markov chain
    else                                                     % In the case that the transition probabilies are all null               
        v_Markov_index(t+1)=v_Markov_index(t);               % The velocity Markov index is kept constant
    end
    
    if v_Markov_index(t+1) > 120
        v_Markov_index(t+1) = 120;                           % Velocity saturation according to highway speed limit (120 km/h)  
    end
    
    dis = sum(S(v_Markov_index(1:t)))/3.6;                         % Update the current distance (from km/h every second to meter/s every second)
    a_highway = (S(v_Markov_index(t+1))-S(v_Markov_index(t)))/3.6; % Update the current highway acceleration (from km/h per second to meter/s per second)
    
    if a_highway > 4                                               % When acceleration is greater than 4.3 m/s^2
        v_Markov_index(t+1) = v_Markov_index(t)+16;                % Velocity increases at most of 15.5 km/h
    elseif a_highway < -7                                          % When deceleration is greater than 6.8 m/s^2    
        v_Markov_index(t+1) = v_Markov_index(t)-25;                % Velocity decreases at most of 24.5 km/h
    end                                                            % Acceleration/deceleration values from the recorded data
    

    [~,index] = min(abs(a_highway-a_state));                       % Chose the closest state among the available ones
    a_highway = a_state(index);                                    % Assign the closest discretized acceleration to the current accelleration

    t = t+1;                                                       % Increase time of 1 second
    
end

v_generate2 = S(v_Markov_index(1:t));                                               % Assign velocity state vector to generated profile 
v_generate2 = [v_generate2 v_generate2(end):-3:0 zeros(1,round(unifrnd(0,1)*20))];  % Terminate the velocity profile 

%% Second urban section

S = v_state;                         % Velocities state space
mu = [1 zeros(1,91)];                % Velocities initial distribution with probability 1 the velocity class at the beginning is the first one                                  
v_Markov_index = rando(mu);          % Generate the first value of state vector
driving_dis = Urban_p2*distance;     % Section driving distance
a_urban = 0;                         % Current urban acceleration initialization
dis = 0;                             % Current duration variable initialization (minutes)
t = 1;                               % Current time variable initialization

while dis <= 0.98*driving_dis*1000   % Compare the current distance with the section distance
       
    [~,indexEntry] = min(abs(a_urban - a_state));     % Find the right discretized acceleration in the acceleration vector
    P = P_ij_combine_u(:,:,indexEntry);               % Select the correct transitions probability matrix
  
    if sum(P(v_Markov_index(t),:))~=0                 % Chek whether or not the transition probabilies are all null  
        if t < 5                                      % For the first 5 times increases the probability of the transition from velocity i-th to i+1-th so that the vehicle accelerates
            P(v_Markov_index(t),v_Markov_index(t)+1) = P(v_Markov_index(t),v_Markov_index(t)+1)+0.5;
        end
        v_Markov_index(t+1) = rando(P(v_Markov_index(t),:)); % Compute the next step of the Markov chain
    else                                              % In the case that the transition probabilies are all null          
        v_Markov_index(t+1) = v_Markov_index(t);      % The velocity Markov index is kept constant  
    end
    
    dis = sum(S(v_Markov_index(1:t)))/3.6;            % Update the current distance (from km/s every second to meters)
    a_urban = (S(v_Markov_index(t+1))-S(v_Markov_index(t)))/3.6; % Update the current urban acceleration (from km/h per second to meter/s per second)
    if a_urban > 5                                    % When acceleration is greater than 5.2 m/s^2
        v_Markov_index(t+1) = v_Markov_index(t)+19;   % Velocity increases at most of 18.7 km/h
    elseif a_urban < -7                               % When deceleration is greater than 6.6 m/s^2
        v_Markov_index(t+1) = v_Markov_index(t)-24;   % Velocity decreases at most of 23.8 km/h
    end                                               % Acceleration/deceleration values from the recorded data
    
    [~,index] = min(abs(a_urban-a_state));            % Chose the closest state among the available ones
    a_urban = a_state(index);                         % Assign the closest discretized acceleration to the current acceleration     
    
    t = t+1;                                          % Increase time of 1 second
    
end

v_generate3 = S(v_Markov_index(1:t));                                               % Assign velocity state vector to generated profile 
v_generate3 = [v_generate3 v_generate3(end):-3:0 zeros(1,round(unifrnd(0,1)*20))];  % Terminate the velocity profile

%% Compose & plot the velocity profile
 
% plot(v_generate,'LineWidth',1)
% xlabel('Time(s)','FontSize',12)
% ylabel('Velocity(km/h)','FontSize',12)

velocityProfile = [v_generate1 v_generate2 v_generate3];