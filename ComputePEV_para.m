function [X_PEV, deadline, complete,SOC0] = ComputePEV_para(Trip_matrix,HHCParameters,Activity,houseNumber)


deadline = zeros(1,size(Activity,2));
complete = zeros(1,size(Activity,2));
% wait = zeros(1,size(Activity,2));
X_PEV = zeros(1,size(Activity,2));
% SOC0 = zeros(1,size(Activity,2));

% Set the initial values
counter = 0;
k = 1;

% When the PEV starts charging at home, we compute the deadline, complete time
% and the waiting time. Those values remained unchanged in the following
% time steps until the PEV is not charged at home.
% X_PEV(k) = 4 when the PEV start charging, otherwise, X_PEV(k) = 0
% The case k=1 is isolated out. The computation is slightly different from
% the other cases.
% The parameters corresponds to k>1 is computed in the for loop

    if ~isempty(find(Trip_matrix(:,1) == k, 1)) 
        xxx = find(Trip_matrix(Trip_matrix(:,1) == k,4)== 4);
        if ~isempty(xxx)
        yyy = find(Trip_matrix(:,1) == k);
        m = yyy(xxx);
        deadline(k) = Trip_matrix(m,2)/10;
        counter = deadline(k)+k;
        complete(k) =  round(abs(1000*6*Trip_matrix(m,6)/HHCParameters.powerPEVHome(houseNumber)));
        X_PEV(k) = 4;
        SOC0(k)  = Trip_matrix(m,7);
        if deadline(k) - complete(k) <=0
            counter  = counter -1;
        end   
        else
            deadline(k) = 0;
            complete(k) = 0;
            counter = 0;
            X_PEV(k) = 0;            
        end
    else
        deadline(k) = 0;
        complete(k) = 0;
        counter = 0;
        X_PEV(k) = 0;
    end

for k = 2: size(Activity,2)
    if ~isempty(find(Trip_matrix(:,1) == k, 1)) 
        xxx = find(Trip_matrix(Trip_matrix(:,1) == k,4) == 4);
        if ~isempty(xxx)
        yyy = find(Trip_matrix(:,1) == k);
        m = yyy(xxx);
        deadline(k) = Trip_matrix(m,2)/10;
        X_PEV(k) = 4;
        counter = deadline(k)+k;
        complete(k) =  round(abs(1000*6*Trip_matrix(m,6)/HHCParameters.powerPEVHome(houseNumber)));
        if deadline(k) - complete(k) <=0
            counter  = counter -1;
        end   
        else
         if k <= counter
            deadline(k) = deadline(k-1);
            complete(k) = complete(k-1);
        else
            deadline(k) = 0;
            complete(k) = 0;
            counter = 0;
         end
        X_PEV(k) = 0;           
        end
    else
        if k <= counter
            deadline(k) = deadline(k-1);
            complete(k) = complete(k-1);
        else
            deadline(k) = 0;
            complete(k) = 0;
            counter = 0;
        end
    X_PEV(k) = 0;
    end
end

SOC0 = Trip_matrix(Trip_matrix(:,6)>0,7)';
