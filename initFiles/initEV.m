% -------------------------------------------------------------------------
% This script initializes the parameters for the EV vehicle
%
% Created:          04/17/2015
% Author:           S.Stockar
% Last Modified:    
%--------------------------------------------------------------------------

% Enviroment
rho = 1.275;    % [kg/m^3]
g   = 9.81;     % [m/s^2]

%-- Vehicle parameters - Nissan Leaf --

% Road Load 
Af          = 2.2;      % Frontal Area [m^2] 
r_w         = 0.33;      % Tire radius [m]
Cd          = 0.42;      % Aerodynamic Coefficient
cr          = 0.012;    % Rolling Resistance [-]

% Transmission
TR          = 2.70;     % Transmission gear ratio
eta_trans   = 0.95;     % Transmission Efficientcy
GR_EM       = 3;        % Electric Motor Gear ratio

% Battery
SOC0        = 0.95;     %Initial SOC
SOC_ref     = 0.25;     % Charge Sustaining level
eta_batt    = 0.95;
BattEmax 	= 24;       % [kWh]  
BattPmax    = 20;       % [kW]
BattVnom    = 3.2;      % Battery Nominal Voltage 

% Electric Motor
EMscale     = 2.5;      % Scale map

% Vechicle Mass
En_density  = 12;       % Assume 12kg/kWh
mbatt       = En_density*BattEmax;
m_veh       = mbatt+1200;     % Vehicle Mass [kg]
