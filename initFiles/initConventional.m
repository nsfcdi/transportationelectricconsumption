% -------------------------------------------------------------------------
% This script initializes the parameters for the PEV vehicle
%
% Created:          04/17/2015
% Author:           S.Stockar
% Last Modified:    
%--------------------------------------------------------------------------

% Enviroment
rho = 1.275;    % [kg/m^3]
g   = 9.81;     % [m/s^2]


%-- Vehicle parameters - Midsize Sedan (Toyota Camry) --

% Road Load
Af  = 2.3;  % Frontal Area [m^2] Reference: (http://ecomodder.com/wiki/index.php/Vehicle_Coefficient_of_Drag_List)
Cd  = 0.32; % Coefficient of Drag [-]
cr  = 0.012;    % Rolling Resistance [-]
r_w = 0.33;      % Tire radius [m]
m_veh       = 1600;     % Vehicle Mass [kg]

% Engine
% Note that the engine fuel consumption maps are expressed in [g/s]
w_IDLE      = 105;    % Idle Speed Control [rad/s]
T_IDLE      = 12;
if engine_type == 1 
    load Gasoline_Parameters
else
    load Diesel_Parameters
end


% Transmission
eta_trans   = 0.95;     % Transmission Efficientcy
TR          = 2.70;     % Transmission gear ratio
ratio_gear = [4.6850    2.9420    1.9230    1.3010    1.0000    1.0000];



