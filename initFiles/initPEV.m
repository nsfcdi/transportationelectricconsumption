% -------------------------------------------------------------------------
% This script initializes the parameters for the PEV vehicle
%
% Created:          04/17/2015
% Author:           S.Stockar
% Last Modified:    
%--------------------------------------------------------------------------

% Enviroment
rho = 1.275;    % [kg/m^3]
g   = 9.81;     % [m/s^2]

% -- Vehicle parameters - Chevy Volt --
load PEV_Parameters

% Road Load 
Af          = 2.16;      % Frontal Area [m^2]
r_w         = 0.33;      % Tire radius [m]
Cd          = 0.42;      % Aerodynamic Coefficient
cr          = 0.012;    % Rolling Resistance [-]

% Transmission
TR          = 2.70;     % Transmission gear ratio
eta_trans   = 0.95;     % Transmission Efficientcy
GR_EM       = 10.946;        % Electric Motor Gear ratio

% Battery
SOC_ref     = 0.25;     % Charge Sustaining level
SOC0        = 0.95;     %Initial SOC
eta_batt    = 0.95;
BattEmax 	= 16;       % [kWh]  
BattPmax    = 20;       % [kW]
BattVnom    = 3.2;      % Battery Nominal Voltage 

% Electric Motor
EMscale     = 3.47;     % 111 kW

% Vechicle Mass
En_density  = 12;       % Assume 12kg/kWh
mbatt       = En_density*BattEmax;
m_veh       = mbatt+1500;     % Vehicle Mass [kg]

% Engine 
w_IDLE      = 800*pi/30;    % Idle Speed Control [rad/s]

p00 =   0.0002756;
p10 =  -9.148e-07;
p01 =  -6.053e-06;
p20 =   3.327e-09;
p11 =   6.795e-08;
p02 =   3.828e-08;

map_w2 = [0:50:7000]*pi/30;
map_T2 = [0:10:400];
[x,y]  = meshgrid(map_w2,map_T2);
map_m_dot2 = p00 + p10.*x + p01.*y + p20.*x.^2 + p11.*x.*y + p02.*y.^2;
